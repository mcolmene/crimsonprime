//server.js
'use strict';
//first we import our dependencies…
const express = require('express');
const bodyParser = require('body-parser');

const emailTemplate =  require('./src/server/emailTemplate');
const userMessage = require('./src/server/userMessage');
const sendMail = require('./src/server/mail');

require('dotenv').config();

const app = express();

const port = process.env.API_PORT || 3000;

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Credentials', 'true');
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
//and remove cacheing so we get the most recent comments
  res.setHeader('Cache-Control', 'no-cache');
  next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send()
});
app.post('/contact', (req, res) => {
  console.log(req.body);
  const message = userMessage(req.body);
  let response = {
    status: 200,
    message: 'Successfully sent email'
  };
  if(typeof message === 'object'){
    response = message;
  } else {
    console.log(message);
    sendMail(message, req.body.email);
  }
  res.send(response);
});


app.listen(port, function() {
  console.log("server running...")
});
