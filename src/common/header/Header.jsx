import React, { Component, PropTypes } from 'react';
import { Row, Col } from 'react-bootstrap/lib';
import { Link } from 'react-router';
import * as url from '../../static/logo.png';
import styles from '../../static/CrimsonPrime.css';

 const Container = () => (
        <Row className={`${styles.menuBar}`}>
          <Col sm={10} md={10} className="center float-l">
            <Col sm={3} md={2}>
              <img src={url} alt="CrimsonPrimeLogo.png"/>
              <Link to="/" className="">Crimson Prime</Link>
            </Col>
            <Col sm={2} md={1}>
              <Link to="/" className="">Home</Link>
            </Col>
            <Col sm={2} md={1}>
              <Link to="/about" className="">About</Link>
            </Col>
            <Col sm={2} md={2}>
              <Link to="/contact" className="">Contact Us</Link>
            </Col>
          </Col>
          <Col sm={2} md={2} className="right float-r">
            <Link to="/login" className="">Login</Link>
          </Col>
        </Row>
    );
export default Container;
