import React from 'react';
import { Col } from 'react-bootstrap';

const Footer = () =>(
  <div className="footer">
    <div className="container">
      <Col xs={12} md={3}>
        {/*logo here*/}
      </Col>
      <Col xs={12} md={3}>
        <ul>
          <li>Contact</li>
          <li>About</li>
          <li>Home</li>
        </ul>
      </Col>
      <Col xs={12} md={3}>
        <ul>
          <li>(602) 476-4189</li>
          <li>Martin Colmenero</li>
          <li>contact@crimsonprime.com</li>
        </ul>
      </Col>
      <Col xs={12} md={3}>
        <ul>
          <li>Follow us</li>
        </ul>
      </Col>
    </div>
  </div>
);

export default Footer;
