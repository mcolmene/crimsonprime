import React, { Component } from 'react';
import Input from 'base-components/build/components/Input';
import { Col, Button, Row } from 'react-bootstrap/lib';

export default class ContactForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      company: '',
      description: '',
      email: '',
      name: '',
      phone: '',
    };
    this.onClick = this.onClick.bind(this);
    this.onChange = this.onChange.bind(this);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onClick() {
    console.log(this.state)
    const myInit = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(this.state)
    };
    fetch('http://localhost:3000/contact', myInit).then((res) => {
      console.log(res);
    })
  }
  render() {
    const { company, description, email, name, phone } = this.state;
    return (
      <Row className="pad-1">
        <Col xs={12}>
          <Input
            value={company}
            name="company"
            placeholder="Company"
            id="company"
            type="text"
            className="form-control margin-tb-2"
            onChange={this.onChange}
          />
        </Col>
        <Col xs={12}>
          <Input
            value={name}
            name="name"
            placeholder="Name"
            id="Name"
            type="text"
            className="form-control margin-tb-2"
            onChange={this.onChange}
          />
        </Col>
        <Col xs={12} md={6}>
          <Input
            value={phone}
            name="phone"
            placeholder="Phone"
            id="phone"
            type="phone"
            className="form-control margin-tb-2"
            onChange={this.onChange}
          />
        </Col>
        <Col xs={12} md={6}>
          <Input
            value={email}
            name="email"
            placeholder="Email"
            id="email"
            type="email"
            className="form-control margin-tb-2"
            onChange={this.onChange}
          />
        </Col>
        <Col xs={12}>
          <Input
            value={description}
            name="description"
            placeholder="Description"
            id="description"
            type="text"
            className="form-control margin-tb-2"
            onChange={this.onChange}
          />
        </Col>
        <Col xs={12}>
          <Button className="btn btn-center btn-primary margin-tb-2" onClick={this.onClick}>Submit</Button>
        </Col>
      </Row>
    );
  };
}
