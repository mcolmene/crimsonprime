import React from 'react';
import ContactForm from './ContactForm';

const Contact = () => (
  <div style={{paddingTop: 75}}>
    <div className="center container-1 box-shadow">
      <h1 className="pad-1">Please Contact us if you have any questions</h1>
      <ContactForm />
    </div>
  </div>
);
export default Contact;
