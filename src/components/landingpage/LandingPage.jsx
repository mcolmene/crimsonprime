import React from 'react';
import { Row, Col } from 'react-bootstrap/lib';
import DetailsBlock  from 'base-components/build/composed/DetailsBlock';
import ImageSection  from 'base-components/build/composed/ImageSection';


import styles from '../../static/CrimsonPrime.css';
const LandingPage = () =>{
  return(
  <div>
    <div className={`${styles.background} parallax`} />
    <Row>
      <Col xs={12} sm={12} md={4}>
        <DetailsBlock />
      </Col>
      <Col xs={12} sm={12} md={4}>
        <DetailsBlock />
      </Col>
      <Col xs={12} sm={12} md={4}>
        <DetailsBlock />
      </Col>
    </Row>
    <ImageSection url="http://cityscaperesidences.com/wp-content/uploads/2016/03/cityscape_2bd1_01.jpg" />
  </div>
)};

export default LandingPage;
