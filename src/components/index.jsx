import React from 'react';

import Header from '../common/header';
import Footer from '../common/footer';

import styles from '../static/CrimsonPrime.css';

const index = ({ children }) =>
  <div>
    <Header />
    <div className="wrapper">
      { children }
    </div>
    <Footer />
  </div>;

export default index;
