const validator =  require('validator');
const errorCodes = require('./errorCodes');

function userMessage(message){
  console.log(errorCodes);
  const {
    company,
    description,
    email,
    interests,
    name,
    phone,
    reference
  } = message;

  // TODO:
  if(!validator.isEmpty(company) && !validator.isAlpha(company))
    return errorCodes.invalid('companyName is not formatted correctly of is empty');

  if(!validator.isAlphanumeric(description))
    return errorCodes.invalid('Description is invalid');

  if(!validator.isEmpty(email) && !validator.isEmail(email))
    return errorCodes.invalid('Email is not formatted correctly or is empty');

  if(!validator.isEmpty(name) && !validator.isAlpha(name))
    return errorCodes.invalid('Name is not formatted correctly of is empty');

  if(!validator.isMobilePhone(phone, 'any'))
    return errorCodes.invalid('phone is invalid');

  //TODO: add validations for interest and reference

  return (
    `Name: ${name},
    \nEmail: ${email},
    \nCompany Name: ${company},
    \nDescription: ${description},
    \nPhone: ${phone}`
  );
}
module.exports = userMessage;
