const nodemailer = require('nodemailer');
const emailTemplate = require('./emailTemplate');

require('dotenv').config();

const sendMail = (message, recipient) => {

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.AUTH_USER,
    pass: process.env.AUTH_PASS
  }
});

// to the user
const userEmail = {
  from: process.env.AUTH_USER,
  to: recipient,
  subject: 'Thank you for reaching out to Crimson Prime!',
  html: emailTemplate
};
// to crimson prime
const companyResponse = {
  from: process.env.AUTH_USER,
  to: process.env.AUTH_USER,
  subject: 'Interest in Crimson Prime Services',
  text: message
};
  transporter.sendMail(userEmail, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
  transporter.sendMail(companyResponse, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log('Email sent: ' + info.response);
    }
  });
};

module.exports = sendMail;
