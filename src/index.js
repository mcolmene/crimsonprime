import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, hashHistory} from 'react-router';

import Index from './components';
import LandingPage from './components/landingpage';
import About from './components/about/About';
import Contact from './components/contact';
import NotFound from './components/404/NotFound';


render(
    <Router history={hashHistory}>
      <Route path="/" component={Index} >
        <IndexRoute component={LandingPage} />
        <Route path="/about" component={About} />
        <Route path="/contact" component={Contact} />
      </Route>
      <Route path="*" component={NotFound} />
    </Router>,
  document.getElementById('root')
);
