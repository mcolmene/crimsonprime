import React from 'react';
import { render } from 'react-dom';

import CrimsonPrime from './src';
import { Provider } from 'react-redux';
import store, { history } from './src/store';


render(
  <Provider store={store}>
    <CrimsonPrime />
  </Provider>,
    document.getElementById('root')
);
