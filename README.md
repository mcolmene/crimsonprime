# CrimsonPrime

## Font types
### Headings are League Spartan, size 42
### Sub-headings are Libre Baskerville, Italic, size 24
### Body text is PT Serif, size 16. It should be comfortably readable for long passages. Serifed fonts are widely used for body text because they are considered easier to read than sans-serif fonts in print. Sans-serif fonts are considered to be more legible on low-resolution computer screens.

## Colors
<code>#ff5c5c</code>
<code>#ffbd4a</code>
<code>#000</code>
<code>#fff</code>
<code>#990000</code>
<code>#2eb2ff</code>